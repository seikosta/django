# -*- coding:utf8 -*-
from django.contrib import admin
from agenda.models  import Pessoa,Carro,Documentos,Setor


#classe de personalizacao da administracao
class PessoaAdmin(admin.ModelAdmin):
    #fields=['nome','endereco','telefone','datanascimento']
    fieldsets=(
               ('Dados Pessoais',{'fields':['nome','endereco','datanascimento'],'classes':['collapse']}),
               (u'Números',{'fields':['telefone'], 'classes':['collapse']})
               )

    
#registrar Pessoa na parte administrativa, PessoaAdmin como parametro
#para personalizar 
admin.site.register(Pessoa,PessoaAdmin) 
admin.site.register(Carro)
admin.site.register(Documentos)
admin.site.register(Setor)