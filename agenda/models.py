# -*- coding: utf8 -*-
from django.db import models
import datetime

# Create your models here.
class Pessoa(models.Model):
    nome=models.CharField(max_length=40)
    telefone=models.CharField(max_length=11)
    endereco=models.TextField()
    datanascimento= models.DateField()
    
    
    def __unicode__(self):
        return "Nome: %s - Endereco: %s " % (self.nome,self.endereco)
    
    def get_anos(self):
        dataatual= datetime.date.today()
        return ((dataatual - self.datanascimento).days/365)
    
# relacionamento 1xM
class Carro(models.Model):
    marca=models.CharField(max_length=40)
    nome=models.CharField(max_length=40)
    pessoa=models.ForeignKey(Pessoa)
    
# relacionamento 1x1    
class Documentos(models.Model):
    rg=models.CharField(max_length=15)
    cpf=models.CharField(max_length=14)
    pessoa =models.OneToOneField(Pessoa)
    
# relacionamento MxN
class Setor(models.Model):
    nome=models.CharField(max_length=40)
    pessoas=models.ManyToManyField(Pessoa)
    
    

    
    